require 'sinatra'
require './order_component/order_component'

registered_entities = [:invoice, :order, :person, :employee, :comapny]
registered_entities.each do |entity|

  get "/#{entity}" do
    erb :entity, :locals => {:registered_entities => registered_entities}
  end

end

configure do
  set :views, ['common/views', 
               'order_component/views']
  # set js files
end

helpers do
  def find_template(views, name, engine, &block)
    views.each { |v| super(v, name, engine, &block) }
  end
end
