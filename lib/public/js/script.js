function myFunction() {
  var input, filter, table, tr, td, i;
  input = document.getElementById("myInput");
  filter = input.value.toUpperCase();
  table = document.getElementById("myTable");
  tr = table.getElementsByTagName("tr");
  for (i = 0; i < tr.length; i++) {
    td = tr[i].getElementsByTagName("td")[0];
    if (td) {
      if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
        tr[i].style.display = "";
      } else {
        tr[i].style.display = "none";
      }
    }
  }
}

function w3_open() {
  localStorage.setItem("sidebar_state", "open");
  document.getElementById("mySidebar").style.display = "block";
}

function w3_close() {
  localStorage.setItem("sidebar_state", "closed");
  document.getElementById("mySidebar").style.display = "none";
}

window.onload = function() {
  if(localStorage.getItem("sidebar_state")==="open") {
    w3_open();
  }
  window.alert(sidebar_state);
};

